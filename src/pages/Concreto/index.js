import React, {useEffect, useState,useRef} from 'react';
import {
        Text,
        View,
        TextInput,
        TouchableOpacity,
        Keyboard,
        SafeAreaView,
        ScrollView,
        StatusBar
    } from 'react-native';

import {Picker} from '@react-native-picker/picker';


import styles from './styles'; 
import { rgbToHex } from '@material-ui/core';


export default function Calculo( {route,navigation} ){

  const [area, setArea] = useState('retangulo');
  const [tipo, setTipo] = useState('macica');
  const [modelo, setModelo] = useState('65');
  const [perda, setPerda] = useState('0.05');

  const [comprimento, setComprimento] = useState('');
  const [medidaComprimento, setMedidaComprimento] = useState('M');

  const [largura, setLargura] = useState('');
  const [medidaLargura, setMedidaLargura] = useState('M');

  const [espessura, setEspessura] = useState('');
  const [medidaEspessura, setMedidaEspessura] = useState('CM');

  const [diametro, setDiametro] = useState('');
  const [medidaDiametro, setMedidaDiametro] = useState('M');

  const [resultadoPreco, setResultadoPreco] = useState('');

  const [preco, setPreco] = useState();

  const [resultado, setResultado] = useState(0);

 
  
  const calcular = ()=>{
      let pode_calcular = false;

    if(comprimento && largura && espessura && area == 'retangulo' && tipo == 'macica'){
        pode_calcular = true;
    }else  if(comprimento && largura && area == 'retangulo' && tipo == 'trelicada'){
        pode_calcular = true;
    }else if(diametro && espessura && area == 'circulo' && tipo == 'macica'){
        pode_calcular = true;
    }else if(diametro && area == 'circulo' && tipo == 'trelicada'){
        pode_calcular = true;
    }

    if(pode_calcular){  
        let volume = 0;
        let comp = parseFloat(comprimento);
        let larg = parseFloat(largura);
        let esp =  parseFloat(espessura);
        if(medidaComprimento == 'CM'){
            comp = comp/100;
        }
        if(medidaLargura == 'CM'){
            larg = largura/100;
        }
        if(medidaEspessura == 'M'){
            esp =  espessura*100;
        }
        if(area === 'retangulo'){
            if(tipo == 'macica'){
                volume = comp * larg * (esp/100);
            }else{
                volume = (comp * larg * modelo) / 1000;
            }
        }

        if(area === 'circulo'){
            let diam = diametro;
            if(medidaDiametro == 'CM'){
                diam = diam/100;
            }
            let raio = diam/2;
            let areaCirculo = Math.PI * (raio*raio);

            if(tipo == 'macica'){
                volume = areaCirculo * (esp/100);
            }else{
                volume = (areaCirculo * modelo) / 1000;
            }
            
        }
       
        volume = volume * (1+parseFloat(perda));
       
        setResultado(Math.fround(volume).toFixed(2));
        if(preco !== ''){
            setResultadoPreco(numberToReal(Math.ceil(volume*preco)));
        }
        Keyboard.dismiss();
    }else{
        alert("Preencha os campos primeiro");
    }
  };

  const numberToReal = (numero) =>{
        numero = numero.toFixed(2).split('.');
        numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
        return numero.join(',');
  };

  useEffect(()=>{
    setComprimento('');
    setLargura('');
    setEspessura('');
    setDiametro('');
  },[area,tipo,modelo]);

  useEffect(()=>{
    setResultado(0);
    },[preco,area,tipo,comprimento,medidaComprimento,largura,medidaLargura,espessura,medidaEspessura,diametro,medidaDiametro]);

  return (

    <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor="#032c85"/>
        <ScrollView>
            <Text numberOfLines={2}  style={styles.title}>Cálculo de Volume de Concreto</Text>

            <View style={styles.row}>
                
                <View style={styles.viewPicker}>
                    <View>
                        <Text style={styles.titlePicker}>Area</Text>
                    </View>
                    <Picker
                        style={styles.pickerComLegenda}
                        selectedValue={area}
                        onValueChange={(itemValue, itemIndex) =>
                            setArea(itemValue)
                        }>
                        <Picker.Item  label="Retângulo" value="retangulo" />
                        <Picker.Item label="Círculo" value="circulo" />
                    </Picker>
                </View>
                <View style={styles.viewPicker}>
                    <View>
                        <Text style={styles.titlePicker}>Tipo</Text>
                    </View>
                    <Picker
                        style={styles.pickerComLegenda}
                        selectedValue={tipo}
                        onValueChange={(itemValue, itemIndex) =>
                            setTipo(itemValue)
                        }>
                        <Picker.Item  label="Maciça" value="macica" />
                        <Picker.Item label="Treliçada" value="trelicada" />
                    </Picker>
                </View>
            </View>

            {(tipo == 'trelicada') && 
            <View style={styles.row}>
                
                <View style={styles.viewPicker}>
                    <View>
                        <Text style={styles.titlePicker}>Modelo</Text>
                    </View>
                    <Picker
                        style={styles.pickerModelo}
                        selectedValue={modelo}
                        onValueChange={(itemValue, itemIndex) =>
                            setModelo(itemValue)
                        }>
                        <Picker.Item  label="H8 - 12cm" value="65" />
                        <Picker.Item  label="H12 - 16cm" value="80" />
                        <Picker.Item  label="H16 - 20cm" value="90" />
                        <Picker.Item  label="H20 - 24cm" value="105" />
                    </Picker>
                </View>
            </View>
            }

            {(area == 'retangulo') && 
            
            <>
                <View style={styles.row}>
                    
                    <View style={styles.viewPicker}>
                        <View style={styles.viewInputComPicker}>
                            <Text style={styles.titlePicker}>Comprimento</Text>
                            <TextInput 
                                style={styles.inputComPiker} 
                                placeholder='Comprimento' 
                                value={comprimento}
                                onChangeText={t=>setComprimento(t)}
                                keyboardType="numeric"
                                placeholderTextColor = '#FFFFFF'
                            />
                        </View>
                        <Picker
                            style={styles.pickerComInput}
                            selectedValue={medidaComprimento}
                            onValueChange={(itemValue, itemIndex) =>
                                setMedidaComprimento(itemValue)
                            }
                        >
                            <Picker.Item label="M" value="M" />
                            <Picker.Item label="CM" value="CM" />
                        </Picker>
                        
                    </View>
                </View>
            
                <View style={styles.row}>
                    
                    <View style={styles.viewPicker}>
                        <View style={styles.viewInputComPicker}>
                            <Text style={styles.titlePicker}>Largura</Text>
                            <TextInput 
                                style={styles.inputComPiker} 
                                placeholder='Largura' 
                                value={largura}
                                onChangeText={t=>setLargura(t)}
                                keyboardType="numeric"
                                placeholderTextColor = '#FFFFFF'
                            />
                        </View>
                        <Picker
                            style={styles.pickerComInput}
                            selectedValue={medidaLargura}
                            onValueChange={(itemValue, itemIndex) =>
                                setMedidaLargura(itemValue)
                            }>
                            <Picker.Item label="M" value="M" />
                            <Picker.Item label="CM" value="CM" />
                        </Picker>
                        
                    </View>
                </View>
            </>
            }
            { area == 'circulo' &&
            <View style={styles.row}>
                
                <View style={styles.viewPicker}>
                    <View style={styles.viewInputComPicker}>
                        <Text style={styles.titlePicker}>Diâmetro</Text>
                        <TextInput 
                            style={styles.inputComPiker} 
                            placeholder='Diâmetro' 
                            value={diametro}
                            onChangeText={t=>setDiametro(t)}
                            keyboardType="numeric"
                            placeholderTextColor = '#FFFFFF'
                        />
                    </View>
                    <Picker
                        style={styles.pickerComInput}
                        selectedValue={medidaDiametro}
                        onValueChange={(itemValue, itemIndex) =>
                            setMedidaDiametro(itemValue)
                        }>
                        <Picker.Item label="M" value="M" />
                        <Picker.Item label="CM" value="CM" />
                    </Picker>
                    
                </View>
            </View>
            }

            {(tipo !== 'trelicada') && 
            <View style={styles.row}>
                <View style={styles.viewPicker}>
                    <View style={styles.viewInputComPicker}>
                        <Text style={styles.titlePicker}>Espessura</Text>
                        <TextInput 
                            style={styles.inputComPiker} 
                            placeholder='Espessura' 
                            value={espessura}
                            onChangeText={t=>setEspessura(t)}
                            keyboardType="numeric"
                            placeholderTextColor = '#FFFFFF'
                        />
                    </View>
                    <Picker
                        style={styles.pickerComInput}
                        selectedValue={medidaEspessura}
                        onValueChange={(itemValue, itemIndex) =>
                            setMedidaEspessura(itemValue)
                        }>
                        <Picker.Item label="CM" value="CM" />
                        <Picker.Item label="M" value="M" />
                    </Picker>
                    
                </View>
            </View>
            }

            <View style={styles.row}>
                
                <View style={styles.viewPicker}>
                    <View style={styles.pickerContent}>
                        <View>
                            <Text style={styles.titlePicker}>Perda %</Text>
                        </View>
                        <Picker
                            style={styles.picker}
                            selectedValue={perda}
                            onValueChange={(itemValue, itemIndex) =>
                                setPerda(itemValue)
                            }>
                            <Picker.Item label="0 %" value="0" />
                            <Picker.Item label="1 %" value="0.01" />
                            <Picker.Item label="2 %" value="0.02" />
                            <Picker.Item label="3 %" value="0.03" />
                            <Picker.Item label="4 %" value="0.04" />
                            <Picker.Item label="5 % (Mais usado)" value="0.05" />
                            <Picker.Item label="6 %" value="0.06" />
                            <Picker.Item label="7 %" value="0.07" />
                            <Picker.Item label="8 %" value="0.08" />
                            <Picker.Item label="9 %" value="0.09" />
                            <Picker.Item label="10 %" value="0.10" />
                            <Picker.Item label="11 %" value="0.11" />
                            <Picker.Item label="12 %" value="0.12" />
                            <Picker.Item label="13 %" value="0.13" />
                            <Picker.Item label="14 %" value="0.14" />
                            <Picker.Item label="15 %" value="0.15" />
                        </Picker>
                    </View>
                </View>
            </View>
            
            <View style={styles.row}>
                <View style={styles.viewInput}>
                    <Text style={styles.label}>O preço é opcional</Text>
                    <TextInput 
                        style={styles.input} 
                        placeholder='Preço por metro cúbico' 
                        value={preco}
                        onChangeText={t=>setPreco(t)}
                        keyboardType="numeric"
                        placeholderTextColor = '#FFFFFF'
                    />  
                </View>
            </View>

            <View style={styles.row}>
                <TouchableOpacity 
                    style={styles.button} 
                    onPress={calcular}
                >
                    <Text style={styles.text}>Calcular</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.rowResultado}>
                { resultado != 0 &&
                    <>
                    <Text style={styles.titleResultado}>Resultado</Text>
                    <Text style={styles.labelResultado}>Metros Cúbicos: {resultado} m³</Text>
                    <Text style={styles.labelResultado}>Valores baseados em uma perda de {perda*100} %</Text>
                    {preco > 0 && <Text style={styles.labelResultado}>Valor: R$ {resultadoPreco}</Text> }
                    </>
                }
            </View>
        </ScrollView>
    </SafeAreaView>
   
  );

}
