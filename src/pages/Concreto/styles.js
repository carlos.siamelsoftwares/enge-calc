import { StyleSheet } from "react-native";

const styles = StyleSheet.create({

    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        backgroundColor: '#023047'
    },
    picker: {
        height: 48, 
        borderColor: 'gray',
        width: '100%', 
        color: '#FFFFFF',
    },
    pickerComLegenda: {
        height: 48, 
        borderColor: 'gray',
        width: '100%', 
        color: '#FFFFFF',
        position: "relative",
        left: -45 
    },
    pickerModelo: {
        height: 48, 
        borderColor: 'gray',
        width: '100%', 
        color: '#FFFFFF',
        position: "relative",
        left: -65 
    },
    pickerContent: {
        flexDirection:'column',width:'100%'
    },
    input: {
        height: 48, 
        borderWidth: 1,
        borderColor: 'gray',  
        padding: 10,
        color: '#FFFFFF'
    },
    inputComPiker: {
        height: 48, 
        color: '#FFFFFF',
        paddingLeft: 4,
        width: '100%',
        justifyContent:'center',
        alignItems:'center',
        paddingStart: 10,
    },
    title: {
        fontSize: 25,
        marginBottom: 20,
        color: '#FFFFFF',
        textAlign:'center'
    },
    row: {
        flexDirection: 'row',
        marginBottom: 15,
        width: '100%',
    },
    label: {
        fontSize: 17,
        marginBottom: 10,
        fontWeight: 'bold',
        color: '#FFFFFF'
    },
    labelResultado: {
        fontSize: 18,
        marginBottom: 10,
        alignSelf: 'flex-start',
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    titleResultado: {
        fontSize: 25,
        marginBottom: 10,
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    viewPicker: {
       flex: 1,
       margin: 5,
       flexDirection: 'row',
       justifyContent: 'space-between',
       borderColor: 'gray', 
       borderWidth: 1,
    },
    viewInputComPicker: {
        width: '70%',    
    },
    pickerComInput: {
        height: 48, 
        borderColor: 'gray',
        width: '30%', 
        color: '#FFFFFF'
    },
    viewInput: {
        width: '100%',
        padding: 5,
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 48,
        backgroundColor: 'blue',
        width: '100%',
        borderRadius: 20
    },
    rowResultado: {
        flex: 1,
        flexDirection: 'column',
        padding: 5, 
        width: '100%'
    },
    text: {
        fontSize: 25,
        color: 'white'
    },
    titlePicker: {
        marginTop:-12,
        paddingLeft:5,
        paddingRight:5, 
        marginBottom: -8,
        backgroundColor: '#023047',
        marginRight: 'auto',
        marginLeft: 5,
        color: '#FFFFFF',
        fontSize: 17,
        fontWeight: 'bold',
        zIndex: 9

    }
});

export default styles;
