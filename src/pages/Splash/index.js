import React, {useEffect, useState,useRef} from 'react';
import {     
        Text,    
        View,
        SafeAreaView,
        Image
    } from 'react-native';

import styles from './styles';


export default function Home( {navigation} ){

    setTimeout(()=>{
        navigation.reset({
            routes: [{name: 'Home'}]
        });
    },3000);
    
  return (

    <SafeAreaView style={styles.container}>
         <Image  source={require("../../../assets/logo.png")} style={styles.logo}/>
        <Text style={styles.title}>Enge. Calc.</Text>
       
        <View style={styles.footer}>
            <Text style={styles.textFooterBold}>Desenvolvido por:</Text>
            <Text style={styles.textFooter}>Gabriel Santos Colares</Text>
            <Text style={styles.textFooter}>e</Text>
            <Text style={styles.textFooter}>Jean Carlos Andrade Ferraz</Text>
        </View>
    </SafeAreaView>
  );
}
