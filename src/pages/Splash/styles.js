import { StyleSheet } from "react-native";

const styles = StyleSheet.create({

  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#023047',
  },
  logo: {
    width: 140,
    height: 40,
    resizeMode: 'stretch',
  },
  title: {
    color: '#ef6727',
    fontSize: 50
  },
  textFooter: {
    color: '#CCCCCC'
  },
  textFooterBold: {
    color: '#17447b',
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 5
  },
  footer: {
    alignItems: 'center'
  }
});

export default styles;
