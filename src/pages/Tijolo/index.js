import React, {useEffect, useState,useRef} from 'react';
import {
        Text,
        View,
        TextInput,
        TouchableOpacity,
        Keyboard,
        SafeAreaView,
        ScrollView,
        StatusBar
    } from 'react-native';

import {Picker} from '@react-native-picker/picker';


import styles from './styles'; 
import { rgbToHex } from '@material-ui/core';


export default function Calculo( {route,navigation} ){

  const [resultado, setResultado] = useState(0);

  const [areaParede,setAreaParede] = useState('');
  const [areaTijolo,setAreaTijolo] = useState('0.0266');
  const [areaItens,setAreaItens] = useState('');

  
  const calcular = ()=>{
      
    if(areaParede <= 0){
        alert("A area da parede precisa ser maior que zero.");
    }else
    if(areaTijolo <= 0){
        alert("A area do tijolo precisa ser maior que zero.");
    }else{
        let area = areaParede - areaItens;
        let resultado = "";
        if(area > 0){
            resultado = Math.ceil(area/areaTijolo);
            setResultado(resultado);
            Keyboard.dismiss();
        }else{
            alert("Impossível calcular.")
        }
    }
    
  };

  useEffect(()=>{
    setResultado('');
  },[areaParede,areaItens,areaTijolo]);


  return (

    <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor="#032c85"/>
        <ScrollView>
            <View>
                <Text style={styles.title}>Cálculo de Tijolos</Text>
            </View>

            <View style={styles.row}>
                <View style={styles.viewInput}>
                    <Text style={styles.titlePicker}>Area da parede(m²)</Text>
                    <TextInput 
                        style={styles.input} 
                        placeholder='Area da parede' 
                        value={areaParede}
                        onChangeText={t=>setAreaParede(t)}
                        keyboardType="numeric"
                        placeholderTextColor = '#FFFFFF'
                    />  
                </View>
            </View>


            <View style={styles.row}>
                <View style={styles.viewInput}>
                    <Text style={styles.titlePicker}> Area de janelas, portas etc.(m²) </Text>
                    <TextInput 
                        style={styles.input} 
                        placeholder='Area de janelas, portas etc.' 
                        value={areaItens}
                        onChangeText={t=>setAreaItens(t)}
                        keyboardType="numeric"
                        placeholderTextColor = '#FFFFFF'
                    />  
                </View>
            </View>

            <View style={styles.row}>
                
                <View style={styles.viewPicker}>
                    <View style={styles.pickerContent}>
                        <View>
                            <Text style={styles.titlePicker}>Tipo de tijolo</Text>
                        </View>
                        <Picker
                            style={styles.picker}
                            selectedValue={areaTijolo}
                            onValueChange={(itemValue, itemIndex) =>
                                setAreaTijolo(itemValue)
                            }>
                            <Picker.Item label="6 FUROS (0,14 x 0,19)" value="0.0266" />
                            <Picker.Item label="8 FUROS (0,19 x 0,19)" value="0.0361" />
                            <Picker.Item label="8 FUROS (0,19 x 0,29)" value="0.0551" />
                            <Picker.Item label="9 FUROS (0,14 x 0,24)" value="0.0336" />
                            <Picker.Item label="INTEIRIÇO (0,09 X 0,19)" value="0.0171" />
                            <Picker.Item label="VAZADO (0,07 x 0,25)" value="0.0175" />
                            <Picker.Item label="ECOLÓGICO (0,625 x 0,25)" value="0.15625" />
                            <Picker.Item label="REFRATÁRIO (0,115 x 0,23)" value="0.02645" />
                            <Picker.Item label="CONCRETO (0,19 x 0,39)" value="0.0741" />
                            <Picker.Item label="VIDRO (0,19 x 0,19)" value="0.0361" />
                            <Picker.Item label="CANALETA CERÂMICA (0,14 x 0,24)" value="0.0336" />
                            <Picker.Item label="LAMINADO 21 FUROS (0,115 x 0,245)" value="0.028175" />
                        </Picker>
                    </View>
                </View>
            </View>

            <View style={styles.row}>
                <TouchableOpacity 
                    style={styles.button} 
                    onPress={calcular}
                >
                    <Text style={styles.text}>Calcular</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.rowResultado}>
                { resultado != 0 &&
                    <>
                    <Text style={styles.titleResultado}>Resultado</Text>
                    <Text style={styles.labelResultado}>A area informada gastará {resultado} tijolos.</Text>
                    </>
                }
            </View>
        </ScrollView>    
    </SafeAreaView>
   
  );

}
