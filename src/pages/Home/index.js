import React, {useEffect, useState,useRef} from 'react';
import {     
        Text,    
        TouchableOpacity,  
        View,
        SafeAreaView,
        Image,
        StatusBar
    } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import styles from './styles';


export default function Home( {navigation} ){

    function calculo(opcao){
        if(opcao == 'concreto')
            navigation.navigate("Concreto",{opcao});
        if(opcao == 'tijolo')
            navigation.navigate("Tijolo",{opcao});
    }

    function sair(){
        navigation.navigate("Home");
    }
    
  return (

    <SafeAreaView>
        <StatusBar backgroundColor="#032c85"/>
        <LinearGradient 
                style={styles.gradient}

                start={{x:0,y:1}}
                end={{x:1,y:0}}
                locations={[.2,0.7]}
                colors={['#6363b5','#033c85']
                }
            >
            

            <View style={styles.itens}>
               <View>
                    <Text style={{fontSize:40,alignSelf:'center',marginBottom:30,color:'#FFFFFF'}}>Enge. Calc.</Text>
                    <Image source={require('../../../assets/icon.png')} style={{width:100,height:100,alignSelf:'center'}}/>
               </View>

                <View>
                    <Text style={styles.title}>Selecione uma Opção</Text>
                    <TouchableOpacity
                        style={styles.itemBotao}
                        onPress={()=>calculo('concreto')}
                    >
                        <Text style={styles.text}>Laje de Concreto</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.itemBotao}
                        onPress={()=>calculo('tijolo')}
                    >
                        <Text style={styles.text}>Tijolos</Text>
                    </TouchableOpacity>
                    
                </View>
            </View>
        </LinearGradient>
    </SafeAreaView>
  );
}
