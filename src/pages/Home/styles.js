import { StyleSheet } from "react-native";

const styles = StyleSheet.create({

  container: {
    flex: 1,
    flexDirection: 'row',
  },
  gradient: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  itemBotao: {
    flex: 1,
    maxHeight: 60,
    backgroundColor: '#3a3944',
    justifyContent:'center',
    alignItems: 'center',
    marginBottom: 20,
    borderRadius: 20
  },
  itens: {
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    height:'100%',
  },
  text: {
    fontSize: 15,
    color: '#fff',
    fontWeight: 'bold'
  },
  title:{
    fontSize: 25,
    marginBottom: 15,
    color: "#fff", 
  }
});

export default styles;
