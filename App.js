import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Home from './src/pages/Home';
import Concreto from './src/pages/Concreto';
import Tijolo from './src/pages/Tijolo';
import Splash from './src/pages/Splash';


const Stack = createStackNavigator();

export default function App(){
    return(
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Splash">
                <Stack.Screen name="Splash" component={Splash} options={{headerShown:false}}/>
                <Stack.Screen name="Home" component={Home} options={{headerShown:false,title:'Calculadora'}}/>
                <Stack.Screen name="Concreto" component={Concreto} options={{headerShown:true,title:'Laje de Concreto',headerTintColor: "#FFFFFF",headerStyle: {backgroundColor:"#032c85"},headerTitleStyle: {color:"#FFFFFF"}}}/>
                <Stack.Screen name="Tijolo" component={Tijolo} options={{headerShown:true,title:'Tijolos',headerTintColor: "#FFFFFF",headerStyle: {backgroundColor:"#032c85"},headerTitleStyle: {color:"#FFFFFF"}}}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}
